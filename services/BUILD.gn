# Copyright (C) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/distributeddatamgr/pasteboard/pasteboard.gni")

config("pasteboard_service_config") {
  visibility = [ ":*" ]
  visibility += [ "//foundation/distributeddatamgr/pasteboard/test/fuzztest/pasteboardservice_fuzzer/*" ]
  include_dirs = [
    "dfx/src",
    "dfx/src/behaviour",
    "dfx/src/statistic",
    "dfx/src/fault",
    "zidl/include",
    "account/include",
    "core/include",
    "load/include",
    "${pasteboard_innerkits_path}/include",
    "${pasteboard_utils_path}/mock/include",
    "${pasteboard_utils_path}/native/include",
    "${pasteboard_utils_path}/system/safwk/native/include",
    "//commonlibrary/c_utils/base/include",
    "//third_party/json/include",
    "//foundation/distributeddatamgr/pasteboard/utils/native/include",
    "//foundation/distributeddatamgr/pasteboard/framework/framework/include",
  ]
  ldflags = [ "-Wl,--exclude-libs=ALL" ]
  cflags = [
    "-fdata-sections",
    "-ffunction-sections",
    "-fvisibility=hidden",
  ]
  cflags_cc = []
  if (os_dlp_part_enabled) {
    cflags_cc += [ "-DWITH_DLP" ]
  }
}

ohos_shared_library("pasteboard_service") {
  sources = [
    "${pasteboard_innerkits_path}/src/paste_data.cpp",
    "${pasteboard_innerkits_path}/src/paste_data_record.cpp",
    "account/src/account_manager.cpp",
    "core/src/dev_manager.cpp",
    "core/src/dev_profile.cpp",
    "core/src/distributed_module_config.cpp",
    "core/src/para_handle.cpp",
    "core/src/pasteboard_dialog.cpp",
    "core/src/pasteboard_service.cpp",
    "dfx/src/behaviour/pasteboard_behaviour_reporter_impl.cpp",
    "dfx/src/calculate_time_consuming.cpp",
    "dfx/src/command.cpp",
    "dfx/src/fault/pasteboard_fault_impl.cpp",
    "dfx/src/hiview_adapter.cpp",
    "dfx/src/pasteboard_dump_helper.cpp",
    "dfx/src/pasteboard_trace.cpp",
    "dfx/src/reporter.cpp",
    "dfx/src/statistic/time_consuming_statistic_impl.cpp",
    "load/src/config.cpp",
    "load/src/loader.cpp",
    "zidl/src/pasteboard_observer_proxy.cpp",
    "zidl/src/pasteboard_service_stub.cpp",
  ]
  public_configs = [
    "//commonlibrary/c_utils/base:utils_config",
    ":pasteboard_service_config",
  ]
  deps = [
    "//foundation/distributeddatamgr/pasteboard/framework/framework:pasteboard_framework",
    "//foundation/distributeddatamgr/pasteboard/framework/tlv:pasteboard_tlv",
    "//foundation/distributeddatamgr/pasteboard/framework/uri:pasteboard_uri",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:wantagent_innerkits",
    "access_token:libaccesstoken_sdk",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "device_info_manager:distributed_device_profile_client",
    "device_manager:devicemanagersdk",
    "eventhandler:libeventhandler",
    "hisysevent_native:libhisysevent",
    "hitrace_native:hitrace_meter",
    "hitrace_native:libhitracechain",
    "hiviewdfx_hilog_native:libhilog",
    "imf:inputmethod_client",
    "init:libbeget_proxy",
    "init:libbegetutil",
    "input:libmmi-client",
    "ipc:ipc_core",
    "multimedia_image_framework:image_native",
    "os_account:os_account_innerkits",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
    "time_service:time_client",
  ]
  if (os_dlp_part_enabled) {
    external_deps += [ "dlp_permission_service:libdlp_permission_sdk" ]
  }
  subsystem_name = "distributeddatamgr"
  part_name = "pasteboard"
}
